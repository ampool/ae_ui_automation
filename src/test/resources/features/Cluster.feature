Feature: Cluster

  Background: 
    Given user navigates to "Dashboard" page

  @sanity
  Scenario: Naviagation to Cluster->Services page
    Given user navigates to "Cluster->Services" page
    And "Services" page is loaded

  @sanity
  Scenario: Naviagation to Cluster->Alerts page
    Given user navigates to "Cluster->Alerts" page
    And "Alerts" page is loaded

  @sanity
  Scenario: Naviagation to Cluster->Backup & Restore page
    Given user navigates to "Cluster->Backup & Restore" page
    And "Backup & Restore" page is loaded

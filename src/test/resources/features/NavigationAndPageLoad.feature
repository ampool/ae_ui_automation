Feature: Navigation and page load to test UI responsiveness

  Background: 
    Given user navigates to "Dashboard" page

  @sanity
  Scenario: Naviagation to Data page
    When user navigates to "Data" page
    Then "Data" page should be loaded

  @sanity
  Scenario: Naviagation to Cluster page
    When user navigates to "Cluster" page
    Then "Cluster" page should be loaded

  @sanity
  Scenario: Naviagation to Notifications page
    When user navigates to "Notifications" page
    Then "Notifications" page should be loaded

  @sanity
  Scenario: Naviagation to Help page
    When user navigates to "Help" page
    Then "Help" page should be loaded

  @sanity
  Scenario: Naviagation to Dashboard page
    When user navigates to "Dashboard" page
    Then "Dashboard" page should be loaded

  @sanity
  Scenario: Naviagation to Data->Data Systems page
    Given user navigates to "Data->Data Systems" page
    And "Data Systems" page is loaded

  @sanity
  Scenario: Naviagation to Data->Data Models page
    Given user navigates to "Data->Data Models" page
    And "Data Models" page is loaded

  @sanity
  Scenario: Naviagation to Data->Query page
    Given user navigates to "Data->Query" page
    And "Query" page is loaded

  @sanity
  Scenario: Naviagation to Data->All Jobs page
    Given user navigates to "Data->All Jobs" page
    And "All Jobs" page is loaded

  @sanity
  Scenario: Naviagation to Cluster->Services page
    Given user navigates to "Cluster->Services" page
    And "Services" page is loaded

  @sanity
  Scenario: Naviagation to Cluster->Alerts page
    Given user navigates to "Cluster->Alerts" page
    And "Alerts" page is loaded

  @sanity
  Scenario: Naviagation to Cluster->Backup & Restore page
    Given user navigates to "Cluster->Backup & Restore" page
    And "Backup & Restore" page is loaded

  @sanity
  Scenario: Execute simple query
    Given user navigates to "Data->Query" page
    And "Query" page is loaded
    When user exeutes a query
    Then query results should appear

Feature: Data

  Background: 
    Given user navigates to "Dashboard" page

  @sanity
  Scenario: Naviagation to Data->Data Systems page
    Given user navigates to "Data->Data Systems" page
    And "Data Systems" page is loaded

  @sanity
  Scenario: Naviagation to Data->Data Models page
    Given user navigates to "Data->Data Models" page
    And "Data Models" page is loaded

  @sanity
  Scenario: Naviagation to Data->Query page
    Given user navigates to "Data->Query" page
    And "Query" page is loaded

  @sanity
  Scenario: Naviagation to Data->All Jobs page
    Given user navigates to "Data->All Jobs" page
    And "All Jobs" page is loaded

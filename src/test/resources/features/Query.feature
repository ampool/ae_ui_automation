Feature: Query

  @sanity
  Scenario: Execute simple query
    Given user navigates to "Data->Query" page
    And "Query" page is loaded
    When user exeutes a query
    Then query results should appear

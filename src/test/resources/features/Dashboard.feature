Feature: Dashboard

  @sanity
  Scenario: Naviagation to Data page
    When user navigates to "Data" page
    Then "Data" page should be loaded

  @sanity
  Scenario: Naviagation to Cluster page
    When user navigates to "Cluster" page
    Then "Cluster" page should be loaded

  @sanity
  Scenario: Naviagation to Notifications page
    When user navigates to "Notifications" page
    Then "Notifications" page should be loaded

  @sanity
  Scenario: Naviagation to Help page
    When user navigates to "Help" page
    Then "Help" page should be loaded

  @sanity
  Scenario: Naviagation to Dashboard page
    When user navigates to "Dashboard" page
    Then "Dashboard" page should be loaded

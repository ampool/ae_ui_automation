/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.steps;

import com.ampool.ae.objects.Page;
import com.ampool.ae.utils.PageNavigator;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class NavigationSteps {

	private PageNavigator pageNavigator = PageNavigator.getPageNavigator();

	@When("^user navigates to \"([^\"]*)\" page$")
	public void user_navigates_to_page(String route) {
		pageNavigator.navigateToPage(route);
	}

	@And("^\"([^\"]*)\" page is loaded$")
	@Then("^\"([^\"]*)\" page should be loaded$")
	public void page_should_be_loaded(String page) {
		pageNavigator.verifyPageLoad(Page.getPage(page));
	}
}

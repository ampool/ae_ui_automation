/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.steps;

import com.ampool.ae.pages.QueryPage;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class QuerySteps {

	private QueryPage queryPage;
	private Config conf = ConfigFactory.load();
	
	public QuerySteps(QueryPage queryPage) {
		this.queryPage = queryPage;
	}

	@When("^user exeutes a query$")
	public void user_executes_a_query() {
		String query = conf.getString("ae.sample.query");
		queryPage.executeQuery(query);
	}
	
	@Then("^query results should appear$")
	public void query_results_should_appear() {
		queryPage.verifyQueryResults();
	}
}

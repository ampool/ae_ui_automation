/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

import java.util.Arrays;

import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.ampool.ae.objects.Page;
import com.ampool.ae.pages.AlertsPage;
import com.ampool.ae.pages.AllJobsPage;
import com.ampool.ae.pages.BackupAndRestorePage;
import com.ampool.ae.pages.ClusterPage;
import com.ampool.ae.pages.DashboardPage;
import com.ampool.ae.pages.DataModelsPage;
import com.ampool.ae.pages.DataPage;
import com.ampool.ae.pages.DataSystemsPage;
import com.ampool.ae.pages.HelpPage;
import com.ampool.ae.pages.NotificationsPage;
import com.ampool.ae.pages.QueryPage;
import com.ampool.ae.pages.ServicesPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class PageNavigator {

	private static final Config conf = ConfigFactory.load();
	private static final Logger log = LoggerFactory.getLogger(PageNavigator.class);
	private static final PageNavigator PAGE_NAVIGATOR = new PageNavigator();
	private static final String Css_Active_Tab = "div[class*='menu'][class*='navbar-fixed-left'] li.active";
	private static final String Css_Active_Link = "ul[class*='nav-tabs tabwhite m-r-15'] li>a[class*='nav-link active']";
	private String[] validTabs = new String[] { "Dashboard", "Data", "Cluster", "Notifications", "Help" };

	DashboardPage dashboardPage;
	DataPage dataPage;
	DataSystemsPage dataSystemsPage;
	DataModelsPage dataModelsPage;
	QueryPage queryPage;
	AllJobsPage allJobsPage;
	ClusterPage clusterPage;
	ServicesPage servicesPage;
	AlertsPage alertsPage;
	BackupAndRestorePage backupAndRestorePage;
	NotificationsPage notificationsPage;
	HelpPage helpPage;

	private long pageLoadStartTime;

	private PageNavigator() {
	}

	public static PageNavigator getPageNavigator() {
		return PAGE_NAVIGATOR;
	}

	public void navigateToPage(String route) {
		String[] navRoute = route.split("->");
		checkForValidNavigationRoute(navRoute);
		String tab = navRoute[0];
		if (navRoute.length == 1)
			navigateToTab(tab);
		else if (navRoute.length == 2)
			navigateToMenu(tab, navRoute[1]);
		else
			Assert.fail("Invalid navigation route " + route + "..!!");
	}

	public void verifyPageLoad(final Page page) {
		String pageName = page.getName();
		try {
			switch (page) {
			case DASHBOARD:
				dashboardPage.waitForPageLoad();
				break;
			case DATA:
				dataPage.waitForPageLoad();
				break;
			case DATASYSTEMS:
				dataSystemsPage.waitForPageLoad();
				break;
			case DATAMODELS:
				dataModelsPage.waitForPageLoad();
				break;
			case QUERY:
				queryPage.waitForPageLoad();
				break;
			case ALLJOBS:
				allJobsPage.waitForPageLoad();
				break;
			case CLUSTER:
				clusterPage.waitForPageLoad();
				break;
			case SERVICES:
				servicesPage.waitForPageLoad();
				break;
			case ALERTS:
				alertsPage.waitForPageLoad();
				break;
			case BACKUPANDRESTORE:
				backupAndRestorePage.waitForPageLoad();
				break;
			case NOTIFICATIONS:
				notificationsPage.waitForPageLoad();
				break;
			case HELP:
				helpPage.waitForPageLoad();
				break;
			default:
				break;
			}
			long timeToLoad = System.currentTimeMillis() - pageLoadStartTime;
			long pageLoadThreshold = conf.getLong("pageload.threshold.milis");
			log.info("{} page is loaded (took {} ms)", pageName, timeToLoad);
			// If any page load threshold is less than difference, then fail the scenario
			if (timeToLoad > conf.getLong("pageload.threshold.milis"))
				log.warn("WARNING: {} TAKES MORE TIME TO LOAD THAN DEFINED THRESHOLD OF {} SECONDS..!!",
						pageName.toUpperCase(), (pageLoadThreshold / 1000));
		} catch (Throwable t) {
			long totalTimeTaken = System.currentTimeMillis() - pageLoadStartTime;
			String error = "Failed to load " + pageName + " page completely. Tried for " + totalTimeTaken + " ms.";
			log.error(error);
			Assert.fail(error + "\n" + t.getMessage());
		}
	}

	private void navigateToTab(String tab) {
		log.info("Index Page: Attempting to load [{}] page", tab);
		String activeTab = getCurrentActiveTab();
		if (tab.equalsIgnoreCase(activeTab)) {
			log.info("Index Page: Currently on [{}] page, skipping page navigation", tab);
			return;
		}
		Page page = Page.getPage(tab);
		clickNavTabElement(page);
		switch (page) {
		case DASHBOARD:
			dashboardPage = page(DashboardPage.class);
			break;
		case DATA:
			dataPage = page(DataPage.class);
			break;
		case CLUSTER:
			clusterPage = page(ClusterPage.class);
			break;
		case NOTIFICATIONS:
			notificationsPage = page(NotificationsPage.class);
			break;
		case HELP:
			helpPage = page(HelpPage.class);
			break;
		default:
			break;
		}
	}

	private void navigateToMenu(String tab, String menu) {
		String activeTab = getCurrentActiveTab();
		if (activeTab.equalsIgnoreCase("Data") || activeTab.equalsIgnoreCase("Cluster")) {
			if (menu.equalsIgnoreCase(getCurrentActiveMenu())) {
				log.info("Index Page: Currently on [" + tab + "->" + menu + "] page, skipping page navigation");
				return;
			}
		} else {
			clickNavTabElement(Page.getPage(tab));
			Page page = Page.getPage(menu);
			clickNavTabElement(page);
			switch (page) {
			case DATASYSTEMS:
				dataSystemsPage = page(DataSystemsPage.class);
				break;
			case DATAMODELS:
				dataModelsPage = page(DataModelsPage.class);
				break;
			case QUERY:
				queryPage = page(QueryPage.class);
				break;
			case ALLJOBS:
				allJobsPage = page(AllJobsPage.class);
				break;
			case SERVICES:
				servicesPage = page(ServicesPage.class);
				break;
			case ALERTS:
				alertsPage = page(AlertsPage.class);
				break;
			case BACKUPANDRESTORE:
				backupAndRestorePage = page(BackupAndRestorePage.class);
				break;
			default:
				break;
			}
		}
	}

	private String getCurrentActiveTab() {
		SelenideElement activeTab = $(Css_Active_Tab);
		activeTab.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"));
		return activeTab.text();
	}

	private String getCurrentActiveMenu() {
		SelenideElement activeMenu = $(Css_Active_Link);
		activeMenu.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"));
		return activeMenu.text();
	}

	private void checkForValidNavigationRoute(String[] navRoute) {
		if (navRoute.length <= 2) {
			boolean isValidTab = false;
			boolean isValidMenu = false;
			for (String tab : validTabs) {
				if (navRoute[0].equalsIgnoreCase(tab)) {
					isValidTab = true;
					break;
				}
			}
			if (!isValidTab)
				Assert.fail("Invalid tab '" + navRoute[0] + "'in navigation route " + Arrays.asList(navRoute));
			else if (navRoute.length == 2) {
				if (navRoute[0].equalsIgnoreCase("Data")) {
					String[] validMenus = new String[] { "Data Systems", "Data Models", "Query", "All Jobs" };
					for (String menu : validMenus) {
						if (navRoute[1].equalsIgnoreCase(menu)) {
							isValidMenu = true;
							break;
						}
					}
				} else if (navRoute[0].equalsIgnoreCase("Cluster")) {
					String[] validMenus = new String[] { "Services", "Alerts", "Backup & Restore" };
					for (String menu : validMenus) {
						if (navRoute[1].equalsIgnoreCase(menu)) {
							isValidMenu = true;
							break;
						}
					}
				}
				if (!isValidMenu)
					Assert.fail("Invalid menu '" + navRoute[1] + "'in navigation route " + Arrays.asList(navRoute));
			}
		} else
			Assert.fail("Invalid navigation route - " + Arrays.asList(navRoute));
	}

	private void clickNavTabElement(final Page page) {
		String selector = page.getSelector();
		String waitFor = page.getWaitFor();
		SelenideElement navElement = null;
		SelenideElement navActiveElement = null;
		if (selector.contains("/"))
			navElement = $(By.xpath(selector));
		else
			navElement = $(selector);
		if (waitFor.contains("/"))
			navActiveElement = $(By.xpath(waitFor));
		else
			navActiveElement = $(waitFor);
		int maxRetry = 5, retry = 0;
		boolean isActionPerformed = false;
		pageLoadStartTime = System.currentTimeMillis();
		do {
			try {
				navElement.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"));
				navElement.waitUntil(Condition.enabled, conf.getLong("selenide.timeout.milis"));
				navElement.click();
				navActiveElement.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"));
				isActionPerformed = true;
				retry = maxRetry + 1;
				break;
			} catch (Throwable t) {
				retry++;
				log.error(t.getMessage());
			}
		} while (retry < maxRetry);
		if ((!isActionPerformed) && retry >= maxRetry)
			Assert.fail("Failed perform click action to navigate to " + page.getName() + " page");
		if (Arrays.asList(validTabs).contains(page.getName())) {
			log.info("Dashboard Page: Clicked on " + page.getName() + " tab");
			log.info("Dashboard Page: " + page.getName() + " tab is active");			
		} else {
			log.info("Dashboard Page: Clicked on " + page.getName()+" menu");
			log.info("Dashboard Page: " + page.getName() + " menu is active");
		}
	}
}

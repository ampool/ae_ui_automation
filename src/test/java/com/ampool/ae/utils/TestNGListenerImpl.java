/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import org.slf4j.MDC;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class TestNGListenerImpl implements IInvokedMethodListener {

	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		String threadName = Thread.currentThread().getName();
		if (threadName.contains("pool-1"))
			threadName = threadName.replaceAll("pool-1", "");
		MDC.put("threadName", threadName);

	}

	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		MDC.remove("threadName");

	}

}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ampool.ae.objects.ScenarioStatus;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

	private static final Logger log = LoggerFactory.getLogger(Hooks.class);
	private String scenarioTitle;
	private String featureName;
	private StringUtils stringUtils = StringUtils.getStringUtils();
	private static int passed, failed, skipped, undefined, pending = 0;
	
	@Before
	public void beforeScenario(Scenario scenario) {
		featureName = stringUtils.capitalizeFirstCharacter(scenario.getId().split(";")[0].replace("-", " "));
		scenarioTitle = scenario.getName();
		log.info("---------------------------------------------------------------------------------------------------");
		log.info("Starts Scenario: " + featureName + " - " + scenarioTitle);
		log.info("---------------------------------------------------------------------------------------------------");
	}

	@After
	public void afterScenario(Scenario scenario) {
		String status = scenario.getStatus();
		log.info(stringUtils.capitalizeFirstCharacter(status + " scenario: " + featureName + " - " + scenarioTitle));
		switch (ScenarioStatus.getStatus(status)) {
		case PASSED:
			passed++;
			break;
		case FAILED:
			failed++;
			break;
		case PENDING:
			pending++;
			break;
		case SKIPPED:
			skipped++;
			break;
		case UNDEFINED:
			undefined++;
			break;
		default:
			break;
		}
		log.info("---------------------------------------------------------------------------------------------------");
		log.info("Execution Summary: PASSED: " + passed + ", FAILED: " + failed + ", SKIPPED: " + skipped
				+ ", PENDING: " + pending + ", UNDEFINED: " + undefined);
		log.info("---------------------------------------------------------------------------------------------------");
	}

}

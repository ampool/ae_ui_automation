/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

import java.io.File;
import java.util.HashMap;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ampool.ae.pages.LoginPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.cucumber.listener.Reporter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class RunnerUtils {

	private static final Logger log = LoggerFactory.getLogger(RunnerUtils.class);
	private static Config conf = ConfigFactory.load();
	private static final String downloadFilePath = System.getProperty("user.dir");

	public static void launchBrowser() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		setChromeOptions(options);
		WebDriver driver = new ChromeDriver(options);
		log.info("Launched chrome browser");
		Dimension dimension = driver.manage().window().getSize();
		log.info("Browser screen size - width: " + dimension.width + ", height: " + dimension.height);
		driver.manage().deleteAllCookies();
		log.info("Deleted all browser cookies");
		setWebDriver(driver);
		setSelenideProperties();
	}

	public static void loginToApplication() {
		long startTime = System.currentTimeMillis();
		log.info("Navigating to url: " + conf.getString("baseurl"));
		LoginPage loginPage = open(conf.getString("baseurl"), LoginPage.class);
		loginPage.waitForPageLoad();
		waitForDocumentComplete();
		log.info("Login page is loaded (took {} ms)", (System.currentTimeMillis() - startTime));
		loginPage.doValidLogin();
	}

	public static void closeBrowser() {
		logout();
		Capabilities capabilities = ((RemoteWebDriver) getWebDriver()).getCapabilities();
		String browserVersion = capabilities.getVersion().toString();
		getWebDriver().quit();
		Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
		Reporter.setSystemInfo("User", System.getProperty("user.name"));
		Reporter.setSystemInfo("Operating System", System.getProperty("os.name"));
		Reporter.setSystemInfo("Browser", "Chrome v" + browserVersion);
		Reporter.setSystemInfo("Applcation", "Ampool Engine v" + conf.getString("version"));
	}

	private static void setSelenideProperties() {
		System.setProperty("selenide.baseUrl", conf.getString("baseurl"));
		System.setProperty("selenide.timeout", conf.getString("selenide.timeout.milis"));
		System.setProperty("selenide.screenshots", conf.getString("selenide.screenshots"));
		System.setProperty("selenide.savePageSource", conf.getString("selenide.savePageSource"));
		System.setProperty("selenide.reports", System.getProperty("user.dir") + conf.getString("selenide.reports"));
	}

	private static void logout() {
		int maxRetry = 5, retry = 0;
		boolean isLogout = false;
		do {
			try {
				$("a.dropdown-toggle").click();
				log.info("Dashboard Page: Clicked on Nav-Link Toggle dropdown");
				SelenideElement logoutLink = $("a>i[class*='fa-sign-out-alt']");
				log.info("Dashboard Page: Logout link visible");
				logoutLink.should(Condition.appears);
				logoutLink.click();
				log.info("Dashboard Page: Clicked on Logout link");
				$("#userNameFormId").should(Condition.appears);
				log.info("Login Page: Username inputbox visible");
				isLogout = true;
				retry = maxRetry + 1;
				log.info("Successfully logged out");
			} catch (Exception e) {
				log.debug(e.getMessage());
				retry++;
			}
		} while (retry < maxRetry);
		if ((!isLogout) && retry >= maxRetry)
			log.error("Failed to logout..!!");
	}

	private static void setChromeOptions(ChromeOptions chromeOpts) {
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", false);
		chromePrefs.put("download.default_directory", downloadFilePath);
		chromeOpts.setExperimentalOption("prefs", chromePrefs);
		if (conf.getBoolean("selenide.chrome.headless")) {
			chromeOpts.addArguments("headless");
			chromeOpts.addArguments("window-size=1920,1040");
			log.info("Chrome browser execution mode : HEADLESS");
		} else {
			chromeOpts.addArguments("start-maximized");
			log.info("Chrome browser execution mode : UI");
		}
		chromeOpts.addArguments("disable-component-update");
		chromeOpts.addArguments("disable-default-apps");
		chromeOpts.addArguments("disable-utilities");
		chromeOpts.addArguments("disable-extensions");
		chromeOpts.addArguments("disable-infobars");
		chromeOpts.addArguments("enable-automation");
		chromeOpts.addArguments("silent");
		chromeOpts.addArguments("incognito");
		chromeOpts.addArguments("disable-background-networking");
		chromeOpts.addArguments("disable-client-side-phishing-detection");
		chromeOpts.addArguments("disable-hang-monitor");
		chromeOpts.addArguments("disable-popup-blocking");
		chromeOpts.addArguments("disable-prompt-on-repost");
		chromeOpts.addArguments("disable-component-extensions-with-background-pages");
		chromeOpts.addArguments("test-type");
		chromeOpts.addArguments("test-type-browser");
		chromeOpts.addArguments("no-sandbox");
		chromeOpts.addArguments("disable-dev-shm-usage");
		chromeOpts.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		chromeOpts.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
	}

	private static void waitForDocumentComplete() {
		new WebDriverWait(getWebDriver(), 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}
}

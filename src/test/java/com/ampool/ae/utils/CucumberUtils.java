/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cucumber.api.DataTable;

public class CucumberUtils {

	private CucumberUtils() {
	}

	private static final Logger log = LoggerFactory.getLogger(CucumberUtils.class);
	private static final CucumberUtils CUCUMBER_UTILS = new CucumberUtils();

	public static CucumberUtils getCucumberUtils() {
		return CUCUMBER_UTILS;
	}

	public DataTable convertToDataTable(Map<String, String> dataMap) {
		List<Map<String, String>> dataMaps = Arrays.asList(dataMap);
		DataTable dataTable = DataTable.create(dataMaps);
		log.debug("Data-table: " + dataTable.toString().trim());
		return dataTable;
	}
}

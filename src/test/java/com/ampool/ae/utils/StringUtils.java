/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringUtils {

	private static final Logger log = LoggerFactory.getLogger(StringUtils.class);
	private static final StringUtils STRING_UTILS = new StringUtils();

	private StringUtils() {
	}

	public static StringUtils getStringUtils() {
		return STRING_UTILS;
	}

	public String capitalizeFirstCharacter(String string) {
		String[] stringArray = string.split(" ");
		StringBuilder enhancedString = new StringBuilder();
		for (String word : stringArray) {
			try {
				enhancedString.append(word.substring(0, 1).toUpperCase()).append(word.substring(1)).append(" ");
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
		return enhancedString.toString().trim();
	}

	public String convertToFormattedText(String line) {
		String[] caseBasedAttribute = line.split("(?<=.)(?=(\\p{Upper}))");
		StringBuilder selectorAttibute = new StringBuilder();
		for (int i = 0; i < caseBasedAttribute.length; i++) {
			if (i == caseBasedAttribute.length - 1)
				selectorAttibute.append(caseBasedAttribute[i]);
			else
				selectorAttibute.append(caseBasedAttribute[i] + " ");
		}
		return selectorAttibute.toString();
	}
}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;
import org.testng.collections.Lists;
import org.testng.collections.Maps;

public class SoftAssert extends Assertion {

	private static final Logger log = LoggerFactory.getLogger(SoftAssert.class);
	private final Map<AssertionError, IAssert<?> > m_errors = Maps.newLinkedHashMap();
	private List<String> m_messages = Lists.newArrayList();

	protected void doAssert(IAssert<?> a) {
		onBeforeAssert(a);
		try {
			a.doAssert();
			onAssertSuccess(a);
			m_messages.add("expected [" + a.getExpected() + "] an found [" + a.getExpected() + "]");
		} catch (AssertionError ex) {
			onAssertFailure(a,ex);
			m_errors.put(ex, a);
		} finally {
			onAfterAssert(a);
		}
	}

	public void asserAll() {
		if (!m_messages.isEmpty()) {
			StringBuilder sb = new StringBuilder("Following asserts passed:");
			boolean first = true;
			for (String m : m_messages) {
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
				sb.append("\n\t");
				sb.append(m);
			}
			log.info(sb.toString());
		}
		if (!m_errors.isEmpty()) {
			StringBuilder sb = new StringBuilder("Expected not matched with found:");
			StringBuilder sbLog = new StringBuilder("Following asserts failed:");
			boolean first = true;
			for (Entry<AssertionError, IAssert<?>> ae : m_errors.entrySet()) {
				if (first) {
					first = false;
				} else {
					sb.append(",\n");
				}
				sb.append("\n\t");
				sbLog.append("\n\t");
				sb.append(ae.getKey().getMessage());
				sbLog.append(ae.getKey().getMessage());
				sb.append("\n\tStack Trace : ");
				sb.append(Arrays.toString(ae.getKey().getStackTrace()).replaceAll(",", "\n\t"));
			}
			log.info(sb.toString());
			throw new AssertionError(sb.toString());
		}
	}

}

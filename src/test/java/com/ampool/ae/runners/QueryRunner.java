/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.runners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.ampool.ae.utils.RunnerUtils;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(strict = true, features = { "src/test/resources/features/Query.feature" }, glue = {
		"com.ampool.ae.steps", "com.ampool.ae.utils" }, plugin = { "pretty", "json:target/automation-reports/json/Query.json",
				"junit:target/automation-reports/junit/Query.xml",
				"com.cucumber.listener.ExtentCucumberFormatter:target/automation-reports/extent/cucumber-extent.html" }, monochrome = true)
public class QueryRunner extends AbstractTestNGCucumberTests {

	private static final Logger log = LoggerFactory.getLogger(QueryRunner.class);
	
	@BeforeClass(alwaysRun = true)
	public static void setUp() {
		log.info("============================================================================================");
		log.info("Starts Feature: Query");
		log.info("============================================================================================");
		RunnerUtils.launchBrowser();
		RunnerUtils.loginToApplication();
	}

	@AfterClass(alwaysRun = true)
	public static void tearDown() {
		RunnerUtils.closeBrowser();
		log.info("============================================================================================");
		log.info("Ends Feature: Query");
		log.info("============================================================================================");
	}

}

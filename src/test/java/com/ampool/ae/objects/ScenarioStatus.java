/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.objects;

public enum ScenarioStatus {

	PASSED("passed"), UNDEFINED("undefined"), PENDING("pending"), SKIPPED("skipped"), FAILED("failed");

	private final String status;

	private ScenarioStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public static ScenarioStatus getStatus(String status) {
		for (ScenarioStatus value : ScenarioStatus.values()) {
			if (status.equalsIgnoreCase(value.getStatus()))
				return value;
		}
		throw new IllegalArgumentException("Unrecognized status: " + status);
	}

}

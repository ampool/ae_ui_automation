/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.objects;

import java.util.HashSet;
import java.util.Set;

public enum FindBy {	
	
	CLASS("class"),
	CSS("css"),
	ID("id"),
	LINKTEXT("linkText"),
	NAME("name"),
	PARTIALLINKTEXT("partialLinkText"),
	TAG("tag"),
	XPATH("xpath");

	private final String strategy;

	private FindBy(String strategy) {
		this.strategy = strategy;
	}

	public String getStrategy() {
		return strategy;
	}

	public static Set<String> getStrategies() {
		Set<String> stategies = new HashSet<String>();
		for (FindBy strategy : FindBy.values())
			stategies.add(strategy.getStrategy());
		return stategies;
	}

	public static FindBy getStatus(String findBy) {
		for (FindBy value : FindBy.values()) {
			if (findBy.equalsIgnoreCase(value.getStrategy()))
				return value;
		}
		throw new IllegalArgumentException("Unrecognized strategy: " + findBy);
	}

}

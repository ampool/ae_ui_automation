/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.objects;

import java.util.HashSet;
import java.util.Set;

public enum Page {

	DASHBOARD("Dashboard", "a>i.fa-lightbulb", "li.active>a>i.fa-lightbulb"),
	DATA("Data", "a>i.fa-newspaper", "li.active>a>i.fa-newspaper"),
	DATASYSTEMS("Data Systems","//a[contains(text(),'Data Systems')]", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Data Systems')]"),
	DATAMODELS("Data Models","//a[contains(text(),'Data Models')]", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Data Models')]"),
	QUERY("Query","//a[contains(text(),'Query')]", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Query')]"),
	ALLJOBS("All Jobs","//a[contains(text(),'All Jobs')]", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'All Jobs')]"),
	CLUSTER("Cluster", "a>i.fa-cubes", "li.active>a>i.fa-cubes"),
	SERVICES("Services", "li>a[href='#Services']", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Services')]"),
	ALERTS("Alerts", "li>a[href='#Alerts']", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Alerts')]"),
	BACKUPANDRESTORE("Backup & Restore", "li>a[href='#Backup_Restore']", "//ul[contains(@class,'nav-tabs tabwhite m-r-15')]/li/a[contains(@class, 'nav-link active')][contains(text(),'Backup & Restore')]"),
	NOTIFICATIONS("Notifications", "a>i.fa-bell", "li.active>a>i.fa-bell"),
	HELP("Help", "a>i.fa-exclamation-triangle", "li.active>a>i.fa-exclamation-triangle");	

	private final String name;
	private final String selector;
	private final String waitFor;

	private Page(String name, String selector, String waitFor) {
		this.name = name;
		this.selector = selector;
		this.waitFor = waitFor;
	}

	public String getName() {
		return name;
	}

	public String getSelector() {
		return selector;
	}

	public String getWaitFor() {
		return waitFor;
	}

	public static Page getPage(String name) {
		for (Page page : Page.values()) {
			if (name.trim().equalsIgnoreCase(page.name))
				return page;
		}
		throw new IllegalArgumentException("Unrecognized page - " + name);
	}

	public static Set<String> getPages() {
		Set<String> pages = new HashSet<String>();
		for (Page page : Page.values())
			pages.add(page.getName());
		return pages;
	}
}

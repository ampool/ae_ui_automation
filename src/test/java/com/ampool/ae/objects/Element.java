/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.objects;

import java.util.HashSet;
import java.util.Set;

public enum Element {

	BUTTON("Button"),
	CHECKBOX("Checkbox"),
	DATEPICKER("Datepicker"),
	DROPDOWN("Dropdown"),
	IMAGE("Image"),
	INPUTBOX("Inputbox"),
	LINK("Link"),
	RADIOBUTTON("Radiobutton"),
	TAG("Tab"),
	LABEL("Label"),
	TEXTAREA("Textarea"),
	FORM("Form"),
	TABLE("Table"),
	BOX("Box"),
	BOXGRID("Boxgrid"),
	ICON("Icon");

	private final String type;
	private Element(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public static Set<String> getTypes() {
		Set<String> types = new HashSet<String>();
		for(Element element: Element.values())
			types.add(element.getType());
		return types;
	}

}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.ampool.ae.objects.Page;
import com.codeborne.selenide.Condition;

public class DataPage extends PageBase {

	private static final String Xpath_DataSystems_Label = "//a[contains(text(),'Data Systems')]";
	private static final String Xpath_DataModels_Label = "//a[contains(text(),'Data Models')]";
	private static final String Xpath_Query_Label = Page.QUERY.getSelector();
	private static final String Xpath_AllJobs_Label = "//a[contains(text(),'All Jobs')]";
	private static final String Css_Active_Tab = "ul[class*='nav-tabs tabwhite m-r-15'] li>a[class*='nav-link active']";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Xpath_DataSystems_Label, Condition.appears);
		wait(Xpath_DataModels_Label, Condition.appears);
		wait(Xpath_Query_Label, Condition.appears);
		wait(Xpath_AllJobs_Label, Condition.appears);
		waitForDocumentComplete();
	}

	public void clickOnQueryTab() {
		click(Xpath_Query_Label, Css_Active_Tab, Condition.text("Query"));
	}
}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class ServicesPage extends PageBase {

	private static final String Xpath_Ampool_Boxgrid = "//app-service//div[contains(@class, 'boxgrid')]//h4[contains(@class, 'theme-text')]/b[contains(text(),'Ampool')]";
	private static final String Xpath_Hive_Boxgrid = "//app-service//div[contains(@class, 'boxgrid')]//h4[contains(@class, 'theme-text')]/b[contains(text(),'Hive')]";
	private static final String Xpath_SparkService_Boxgrid = "//app-service//div[contains(@class, 'boxgrid')]//h4[contains(@class, 'theme-text')]/b[contains(text(),'SPARK_SERVICE')]";
	private static final String Xpath_KafkaConnect_Boxgrid = "//app-service//div[contains(@class, 'boxgrid')]//h4[contains(@class, 'theme-text')]/b[contains(text(),'Kafka_Connect')]";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Xpath_Ampool_Boxgrid, Condition.appears);
		wait(Xpath_Hive_Boxgrid, Condition.appears);
		wait(Xpath_SparkService_Boxgrid, Condition.appears);
		wait(Xpath_KafkaConnect_Boxgrid, Condition.appears);
		waitForDocumentComplete();
	}
}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class QueryPage extends PageBase {

	private static final String Css_SampleQuery_Textarea = "textarea[placeholder='Sample Query']";
	private static final String Xpath_Catalog_Label = "//div[text()='Catalog']";
	private static final String Xpath_Schema_Label = "//div[text()='Schema']";
	private static final String Css_ExecuteQuery_Button = "button[title='Execute query']";
	private static final String Css_Spinner_Icon = "div[class*='fa-spinner fa-spin text-center']";
	private static final String Xpath_Result_Label = "//a[text()='Result']";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_SampleQuery_Textarea, Condition.appears);
		wait(Xpath_Catalog_Label, Condition.appears);
		wait(Xpath_Schema_Label, Condition.appears);
		wait(Css_ExecuteQuery_Button, Condition.appears);
		waitForDocumentComplete();
	}

	public void executeQuery(String query) {
		setValue(Css_SampleQuery_Textarea, query);
		click(Css_ExecuteQuery_Button);
		wait(Css_Spinner_Icon, Condition.appears);
	}

	public void verifyQueryResults() {
		wait(Css_Spinner_Icon, Condition.disappears);
		wait(Xpath_Result_Label, Condition.appears);
	}
}

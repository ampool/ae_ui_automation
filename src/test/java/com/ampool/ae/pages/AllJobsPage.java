/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class AllJobsPage extends PageBase {

	private static final String Css_RefreshJobsList_Icon = "i[title='Refresh jobs list'][class*='fa-sync-alt']";
	private static final String Css_SearchJobByName_Inputbox = "input[placeholder='Search Job by Name']";
	private static final String Css_Data_Table = "table[class*='table-data']";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_RefreshJobsList_Icon, Condition.appears);
		wait(Css_SearchJobByName_Inputbox, Condition.appears);
		wait(Css_Data_Table, Condition.appears);
		waitForDocumentComplete();
	}
}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class DashboardPage extends PageBase {

	private static final String Css_WelcomeMessage_Label = "h5";
	private static final String Xpath_Query_Link = "//a/span[contains(text(),'Query')]";
	private static final String Xpath_SusscribeToDataSystems_Link = "//a/span[contains(text(),'Subscribe to Data System')]";
	private static final String Xpath_PrestoUI_Link = "//a/span[contains(text(),'Presto UI')]";
	private static final String Css_NavLinkToggle_Dropdown = "a.dropdown-toggle";
	private static final String Css_Logout_Link = "a>i[class*='fa-sign-out-alt']";
	private static final String Css_Username_Inputbox = "#userNameFormId";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_WelcomeMessage_Label, Condition.appears);
		wait(Xpath_Query_Link, Condition.appears);
		wait(Xpath_SusscribeToDataSystems_Link, Condition.appears);
		wait(Xpath_PrestoUI_Link, Condition.appears);
		wait(Css_NavLinkToggle_Dropdown, Condition.appears);
		waitForDocumentComplete();
	}

	public void logout() {
		click(Css_NavLinkToggle_Dropdown);
		click(Css_Logout_Link);
		wait(Css_Username_Inputbox, Condition.appears);
		waitForDocumentComplete();
	}

}

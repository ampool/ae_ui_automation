/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class NotificationsPage extends PageBase {

	private static final String Css_Notifications_Label = "h5";
	private static final String Css_DataTable_Box = "table[class*='table-data']";
	private static final String Xpath_Clear_Button = "//button[contains(text(),'Clear')]";
	private static final String Xpath_Filter_Inputbox = "//span[contains(text(),'Filter')][contains(@class, 'ng-star-inserted')]";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_Notifications_Label, Condition.appears);
		wait(Css_Notifications_Label, Condition.text("Notifications"));
		wait(Css_DataTable_Box, Condition.appears);
		wait(Xpath_Clear_Button, Condition.appears);
		wait(Xpath_Filter_Inputbox, Condition.appears);
		waitForDocumentComplete();
	}
}

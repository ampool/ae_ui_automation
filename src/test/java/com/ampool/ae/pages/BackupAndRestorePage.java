/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class BackupAndRestorePage extends PageBase {

	private static final String Xpath_BackupThemeText_Label = "//h4[@class='theme-text'][contains(text(),'Backup')]";
	private static final String Xpath_RestorePrimaryText_Label = "//h4[@class='text-primary'][contains(text(),'Restore')]";
	private static final String Css_TakeBackupOfCluster_Button = "button[title='Take backup of cluster']";
	private static final String Css_RestoreBackupOfCluster_Button = "button[title='Restore cluster from backup']";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Xpath_BackupThemeText_Label, Condition.appears);
		wait(Xpath_RestorePrimaryText_Label, Condition.appears);
		wait(Css_TakeBackupOfCluster_Button, Condition.appears);
		wait(Css_RestoreBackupOfCluster_Button, Condition.appears);
		waitForDocumentComplete();
	}
}

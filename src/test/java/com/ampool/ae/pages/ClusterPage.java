/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.ampool.ae.objects.Page;
import com.codeborne.selenide.Condition;

public class ClusterPage extends PageBase {

	private static final String Css_Host_Link = "li>a[href='#Host']";
	private static final String Css_Services_Link = Page.SERVICES.getSelector();
	private static final String Css_Alerts_Link = Page.ALERTS.getSelector();
	private static final String Css_BackupAndRestore_Link = Page.BACKUPANDRESTORE.getSelector();

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_Host_Link, Condition.appears);
		wait(Css_Services_Link, Condition.appears);
		wait(Css_Alerts_Link, Condition.appears);
		wait(Css_BackupAndRestore_Link, Condition.appears);
		waitForDocumentComplete();
	}
}

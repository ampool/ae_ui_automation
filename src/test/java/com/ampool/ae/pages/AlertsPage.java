/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class AlertsPage extends PageBase {

	private static final String Xpath_AlertsForService_Label = "//div[contains(text(),'Alert for Service')]";
	private static final String Css_FilterForAlertDetails_Inputbox = "input[placeholder*='Filter for alert details']";
	private static final String Xpath_Actions_Button = "//button[contains(text(),'Actions')]";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Xpath_AlertsForService_Label, Condition.appears);
		wait(Css_FilterForAlertDetails_Inputbox, Condition.appears);
		wait(Xpath_Actions_Button, Condition.appears);
		waitForDocumentComplete();
	}
}

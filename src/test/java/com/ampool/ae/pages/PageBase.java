/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.screenshot;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.ampool.ae.objects.Element;
import com.ampool.ae.objects.FindBy;
import com.ampool.ae.utils.StringUtils;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.cucumber.listener.Reporter;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public abstract class PageBase {

	Config conf = ConfigFactory.load();
	protected static final StringUtils STRING_UTILS = StringUtils.getStringUtils();
	private static final Logger log = LoggerFactory.getLogger(PageBase.class);

	public abstract HashMap<String, String> getAttributeMap();

	public abstract void waitForPageLoad();

	private Hashtable<String, String> getElementAttributes(String callerClass, String locator) {
		Hashtable<String, String> elementAttributes = new Hashtable<String, String>();
		try {
			String elementConstant = null;
			@SuppressWarnings("deprecation")
			Object obj = Class.forName(callerClass).newInstance();
			@SuppressWarnings("unchecked")
			HashMap<String, String> attributeMap = (HashMap<String, String>) Class.forName(callerClass)
					.getMethod("getAttributeMap", new Class[] {}).invoke(obj);
			for (Entry<String, String> entry : attributeMap.entrySet()) {
				if (Objects.equals(locator, entry.getValue()))
					elementConstant = entry.getKey();
			}
			validateElementString(elementConstant);
			List<String> elementConstantList = Arrays.asList(elementConstant.split("_"));
			elementAttributes.put("String", elementConstant);
			elementAttributes.put("By", elementConstantList.get(0));
			elementAttributes.put("Name", elementConstantList.get(1));
			elementAttributes.put("Type", elementConstantList.get(2));
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		return elementAttributes;
	}

	private void validateElementString(String elementString) {
		StringBuilder msg = new StringBuilder();
		if (elementString.endsWith("_"))
			Assert.fail(
					msg.append("For enhanced logging feature, selector string should not ends with '_' delimiter. '")
							.append(elementString).append(" ' is ending with '_'.").toString());
		List<String> elementAttributes = Arrays.asList(elementString.split("_"));
		Set<String> elementTypes = Element.getTypes();
		Set<String> findStrategies = FindBy.getStrategies();
		if (elementAttributes.size() != 3)
			Assert.fail(msg.append("For enhanced logging feature, selector string should have only 2 '_' delimiters. '")
					.append(elementString).append("' is having ").append(elementAttributes.size()).toString());
		if (!elementTypes.contains(elementAttributes.get(2)))
			Assert.fail(msg.append("For enhanced logging feature, selector type should be from ").append(elementTypes)
					.append(". '").append(elementAttributes.get(2)).append("' is not as per listed selector types")
					.toString());
		boolean validFindBy = false;
		for (String strategy : findStrategies) {
			if (strategy.equalsIgnoreCase(elementAttributes.get(0))) {
				validFindBy = true;
				break;
			}
		}
		if (!validFindBy)
			Assert.fail(msg.append("For accurate web-element identification, selector find by strategy should be from ")
					.append(validFindBy).append(". '").append(elementAttributes.get(0))
					.append("' is not from the accepted startegied ").append(findStrategies).toString());
		for (int i = 1; i < elementAttributes.size(); i++) {
			boolean caseCheck = Character.isUpperCase(elementAttributes.get(i).charAt(0));
			if (!caseCheck) {
				if (i == 1)
					Assert.fail(msg.append(
							"For enhanced logging feature, first character of selector string should starts with upper case. '")
							.append(elementAttributes.get(1)).append("' is not starting with upper case.").toString());
				if (i == 2)
					Assert.fail(msg
							.append("For enhanced logging feature, selector find by should starts with upper case. '")
							.append(elementAttributes.get(2)).append("' is not starting with upper case.").toString());
			}
		}
	}

	public SelenideElement findElement(String findBy, String selector) {
		SelenideElement element = null;
		switch (FindBy.getStatus(findBy)) {
		case CLASS:
			element = $(By.className(selector));
			break;
		case CSS:
			element = $(By.cssSelector(selector));
			break;
		case ID:
			element = $(By.id(selector));
			break;
		case LINKTEXT:
			element = $(By.linkText(selector));
			break;
		case PARTIALLINKTEXT:
			element = $(By.partialLinkText(selector));
			break;
		case NAME:
			element = $(By.name(selector));
			break;
		case TAG:
			element = $(By.tagName(selector));
			break;
		case XPATH:
			element = $(By.xpath(selector));
			break;
		default:
			break;
		}
		return element;
	}

	public ElementsCollection findElements(String findBy, String selector) {
		ElementsCollection elements = null;
		switch (FindBy.getStatus(findBy)) {
		case CLASS:
			elements = $$(By.className(selector));
			break;
		case CSS:
			elements = $$(By.cssSelector(selector));
			break;
		case ID:
			elements = $$(By.id(selector));
			break;
		case LINKTEXT:
			elements = $$(By.linkText(selector));
			break;
		case PARTIALLINKTEXT:
			elements = $$(By.partialLinkText(selector));
			break;
		case NAME:
			elements = $$(By.name(selector));
			break;
		case TAG:
			elements = $$(By.tagName(selector));
			break;
		case XPATH:
			elements = $$(By.xpath(selector));
			break;
		default:
			break;
		}
		return elements;
	}

	private void attachScreenshot(String screenshot) {
		try {
			Reporter.addScreenCaptureFromPath(screenshot);
		} catch (Exception e) {
		}
	}

	private void addLogs(String statement, String logLevel) {
		if (logLevel.equalsIgnoreCase("INFO")) {
			log.info(statement);
			try {
				Reporter.addStepLog("-\t" + statement);
			} catch (Exception e) {
			}
		} else if (logLevel.equalsIgnoreCase("DEBUG"))
			log.debug(statement);
		else
			log.error(statement);
	}

	public void wait(String selector, Condition condition) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		int maxRetry = conf.getInt("element.load.max.retry"), retry = 0;
		boolean isLoaded = false;
		String error = null;
		do {
			try {
				findElement(elementAttributes.get("By"), selector).waitUntil(condition,
						conf.getLong("selenide.timeout.milis"));
				isLoaded = true;
				retry = maxRetry + 1;
				break;
			} catch (Throwable t) {
				error = t.getMessage();
				retry++;
			}
		} while (retry < maxRetry);
		if ((!isLoaded) && retry >= maxRetry) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			error = msg.append(pageName).append(": ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append(" is not loaded within default timeout; Error: ")
					.append(error).append(" . For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).append(" ").append(condition).toString(), "INFO");
	}

	public void wait(String selector, Condition condition, long timeout) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		int maxRetry = conf.getInt("element.load.max.retry"), retry = 0;
		boolean isLoaded = false;
		String error = null;
		do {
			try {
				findElement(elementAttributes.get("By"), selector).waitUntil(condition, timeout);
				isLoaded = true;
				retry = maxRetry + 1;
				break;
			} catch (Throwable t) {
				error = t.getMessage();
				retry++;
			}
		} while (retry < maxRetry);
		if ((!isLoaded) && retry >= maxRetry) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			error = msg.append(pageName).append(": ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append(" is not loaded within ").append(timeout)
					.append(" timeout; Error: ").append(error)
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).append(" ").append(condition).toString(), "INFO");
	}

	public void shouldHave(String selector, Condition condition) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		int maxRetry = conf.getInt("element.load.max.retry"), retry = 0;
		boolean isConditionMatcher = false;
		String error = null;
		do {
			try {
				findElement(elementAttributes.get("By"), selector).shouldHave(condition);
				isConditionMatcher = true;
				retry = maxRetry + 1;
				break;
			} catch (Throwable t) {
				error = t.getMessage();
				retry++;
			}
		} while (retry < maxRetry);
		if ((!isConditionMatcher) && retry >= maxRetry) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			error = msg.append(pageName).append(": ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append(" has not ").append(condition).append("; Error: ")
					.append(error).append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).append(" has ").append(condition).toString(), "INFO");
	}

	public void clear(String selector) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).clear();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to clear ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Cleared ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
	}

	public void click(String selector) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"))
					.waitUntil(Condition.enabled, conf.getLong("selenide.timeout.milis")).click();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to click ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Clicked ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
	}

	public void click(String selector1, String selector2, Condition condition) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector1);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		int maxRetry = conf.getInt("element.load.max.retry"), retry = 0;
		boolean isClickSuccess = false;
		String error = null;
		do {
			try {
				findElement(elementAttributes.get("By"), selector1)
						.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis"))
						.waitUntil(Condition.enabled, conf.getLong("selenide.timeout.milis")).click();
				findElement(elementAttributes.get("By"), selector2).waitUntil(condition,
						conf.getLong("selenide.timeout.milis"));
				isClickSuccess = true;
				retry = maxRetry + 1;
				break;
			} catch (Throwable t) {
				error = t.getMessage();
				retry++;
			}
		} while (retry < maxRetry);
		if ((!isClickSuccess) && retry >= maxRetry) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			error = msg.append(pageName).append(": Failed to click ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(error)
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Clicked ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
	}

	public String getText(String selector) {
		String text = null;
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			text = findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).text();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to get text from ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Got ").append(text).append(" from ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
		return text;
	}

	public List<String> getTexts(String selector) {
		List<String> texts = new ArrayList<String>();
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			findElement(elementAttributes.get("By"), selector).waitUntil(Condition.appears,
					conf.getLong("selenide.timeout.milis"));
			texts = findElements(elementAttributes.get("By"), selector).texts();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to get texts from ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Got ").append(texts).append(" from ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
		return texts;
	}

	public String getValue(String selector) {
		String value = null;
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			value = findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).getValue();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to get value from ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Got ").append(value).append(" from ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
		return value;
	}

	public String getAttribute(String selector, String attribute) {
		String attributeValue = null;
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			attributeValue = findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).getAttribute(attribute);
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to get ").append(attribute).append(" from ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Got ").append(attributeValue).append(" ").append(attribute)
				.append(" from ").append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
		return attributeValue;
	}

	public void setValue(String selector, String value) {
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).setValue(value);
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": Failed to set value ").append(value).append(" in ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append("; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		addLogs(msg.append(pageName).append(": Set ").append(value).append(" in ")
				.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
				.append(elementAttributes.get("Type")).toString(), "INFO");
	}

	public boolean isEnabled(String selector) {
		boolean enabled = false;
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			enabled = findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).isEnabled();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append(" is not enabled; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		if (enabled)
			addLogs(msg.append(pageName).append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name")))
					.append(" ").append(elementAttributes.get("Type")).append(" is enabled").toString(), "INFO");
		else
			addLogs(msg.append(pageName).append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name")))
					.append(" ").append(elementAttributes.get("Type")).append(" is not enabled").toString(), "INFO");
		return enabled;
	}

	public boolean isVisible(String selector) {
		boolean visible = false;
		StringBuilder msg = new StringBuilder();
		String callerClass = new Exception().getStackTrace()[1].getClassName();
		Hashtable<String, String> elementAttributes = getElementAttributes(callerClass, selector);
		String pageName = STRING_UTILS.convertToFormattedText(callerClass.replace("com.ampool.ae.pages.", "").trim());
		try {
			visible = findElement(elementAttributes.get("By"), selector)
					.waitUntil(Condition.appears, conf.getLong("selenide.timeout.milis")).isDisplayed();
		} catch (Throwable t) {
			attachScreenshot(screenshot(elementAttributes.get("String")));
			String error = t.getMessage();
			error = msg.append(pageName).append(": ")
					.append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name"))).append(" ")
					.append(elementAttributes.get("Type")).append(" is not visible; Error: ").append(t.getMessage())
					.append(". For more details refer screenshot in automation-reports/extent/cucumber-extent.html").toString();
			addLogs(error, "ERROR");
			Assert.fail(error);
		}
		if (visible)
			addLogs(msg.append(pageName).append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name")))
					.append(" ").append(elementAttributes.get("Type")).append(" is visible").toString(), "INFO");
		else
			addLogs(msg.append(pageName).append(STRING_UTILS.convertToFormattedText(elementAttributes.get("Name")))
					.append(" ").append(elementAttributes.get("Type")).append(" is not visible").toString(), "INFO");
		return visible;
	}

	public static void waitForDocumentComplete() {
		new WebDriverWait(getWebDriver(), 30).until(webDriver -> ((JavascriptExecutor) webDriver)
				.executeScript("return document.readyState").equals("complete"));
	}

}

/*
 * COPYRIGHT. AMPOOL INC 2021. ALL RIGHTS RESERVED.
 * 
 * This software is only to be used for the purpose for which it has been
 * provided. No part of it is to be reproduced, disassembled, transmitted,
 * stored in a retrieval system or translated in any human or computer
 * language in any way or for any other purposes whatsoever without the
 * prior written consent of Ampool Inc.
 */
package com.ampool.ae.pages;

import static com.codeborne.selenide.Selenide.page;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.codeborne.selenide.Condition;

public class LoginPage extends PageBase {

	private static final String Css_Username_Inputbox = "#userNameFormId";
	private static final String Css_Password_Inputbox = "#passwordFormId";
	private static final String Xpath_Login_Button = "//button[contains(text(),'LOGIN')]";

	@Override
	public HashMap<String, String> getAttributeMap() {
		List<Field> fieldList = Arrays.asList(this.getClass().getDeclaredFields());
		HashMap<String, String> fieldMap = new HashMap<String, String>();
		try {
			for (Field field : fieldList) {
				fieldMap.put(field.getName(), field.get(field).toString());
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return fieldMap;
	}

	@Override
	public void waitForPageLoad() {
		wait(Css_Username_Inputbox, Condition.appears);
		wait(Css_Password_Inputbox, Condition.appears);
		wait(Xpath_Login_Button, Condition.appears);
		waitForDocumentComplete();
	}

	public DashboardPage doValidLogin() {
		setValue(Css_Username_Inputbox, conf.getString("username"));
		setValue(Css_Password_Inputbox, conf.getString("password"));
		click(Xpath_Login_Button);
		return page(DashboardPage.class);
	}

}
